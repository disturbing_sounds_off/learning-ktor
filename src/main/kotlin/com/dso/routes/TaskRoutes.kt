package com.dso.routes

import com.dso.models.Task
import com.dso.models.taskStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.taskRouting(){
    route("/task") {
        get {
            if (taskStorage.isNotEmpty())
                call.respond(taskStorage)
            else
                call.respondText("No tasks exist yet", status = HttpStatusCode.OK)
        }

        get("{id?}") {
            val id = call.parameters["id"] ?: return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )

            val task = taskStorage.find { it.id == id.toInt() } ?: return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.NotFound
            )

            call.respond(task)
        }

        post {
            val task = call.receive<Task>()
            taskStorage.add(task)
            call.respondText(
                "Task added correctly",
                status = HttpStatusCode.Created
            )
        }

        delete("{id?}") {
            val id = call.parameters["id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)

            if (taskStorage.removeIf{it.id == id.toInt()})
                call.respondText ( "Task removed correctly", status = HttpStatusCode.Accepted )
            else
                call.respondText("Not Found", status = HttpStatusCode.NotFound)
        }

    }
}
