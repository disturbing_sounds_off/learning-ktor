package com.dso.plugins

import com.dso.routes.taskRouting
import io.ktor.server.routing.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
        taskRouting()
    }
}
