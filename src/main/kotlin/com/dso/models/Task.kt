package com.dso.models

import kotlinx.serialization.Serializable

@Serializable
data class Task (
    val id: Int,
    val title: String,
    val content: String,
    val isDone: Boolean
        )

val taskStorage = mutableListOf(
    Task(
        1,
        "Test task",
        "This is an example of task, that is going to be used for testing purposes",
        false
    )
)